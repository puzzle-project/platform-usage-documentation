.. PUZZLE documentation master file, created by
   sphinx-quickstart on Fri Jul 21 14:19:49 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the online documentation of PUZZLE Project and PUZZLE Platform!
=======================================================


What is PUZZLE!
------------

PUZZLE brings together multidisciplinary competences and resources from the academia, industry and research community focusing on digital community, multi-dependency cyberphysical risk assessment, edge trust assurance services and remote attestation, distributed processing, programmable networking mechanisms, cybersecurity analytics, deep analysis and distributed machine learning, threat intelligence and blockchain technologies.


.. raw:: html
   
	<iframe width="560" height="315" src="https://www.youtube.com/embed/smLbnl66TCo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



PUZZLE Platform
------------
PUZZLE Platform has been developed to protect Kubernetes based clusters and applications deployed in them. It has two main parts, one local and one online. The "core" part of PUZZLE for privacy reasons can be installed in the local premises that need to be protected; this core part of the platform is accessible for the users via the PUZZLE Dashboard. The other part is the publicly accessible and centralized PUZZLE marketplace.


.. image:: UsageGuide/assets/market-platform.png


PUZZLE Dashboard
------------

The central point of interaction of with PUZZLE Platform, where people responsible for cybersecurity issues management within SMEs are able to protect their assets and receive information regarding vulnerabilities, identified threats and risks for their infrustructure and applications. 


PUZZLE Marketplace
------------

PUZZLE provides a hub for finding up-to-date cybersecurity mechanisms as a service. By using configurable policy templates, PUZZLE adopters can download the policy templates in their local setup of PUZZLE Platform and protect their assets.   





Online Documentation Contents
=====================================================================

.. toctree::
   :maxdepth: 2


   :caption: Platform Installation
   
   UsageGuide/a_puzzle_platform_setup
      


   :caption: Providing Services in PUZZLE Marketplace
   
   UsageGuide/b_getting_started_with_puzzle
   UsageGuide/c_using_marketplace


.. toctree::
  :maxdepth: 2
  :caption: Support
  
  support
